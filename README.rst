Anicat
======

Introduction
------------
Anicat is a programming written in C, surpporting static typing.

Source Tree
-----------

+-------+------------------+
|Catalog|Description       |
+=======+==================+
|doc    |Documentations.   |
+-------+------------------+
|include|C headers.        |
+-------+------------------+
|lib    |Libraries.        |
+-------+------------------+
|src    |Source code.      |
+-------+------------------+
|test   |Scripts for tests.|
+-------+------------------+

Build
-----
.. code-block:: shell

 $ mkdir build && cd build
 $ cmake ..
 $ make #build

Bugs & Reports
--------------
You can report a bug or share your ideas by E-mail **im31core@yandex.com**.
