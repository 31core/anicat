#include <stdlib.h>
#include <src/vm.h>

void vram_init(VRAM *vram, v_addr addr, size_t size)
{
	vram->p_addr = malloc(size);
	vram->v_addr = addr;
	vram->v_size = size;
}

/* copy data to vram */
size_t vram_copy_to(VRAM *vram, const void *buf, v_addr addr, size_t size)
{
	if(addr > vram->v_addr + vram->v_size)
	{
		return 0;
	}
	else if(addr + size > vram->v_addr + vram->v_size)
	{
		size = vram->v_addr + vram->v_size - addr;
	}
	uint8_t *p_addr = vram->p_addr + (addr - vram->v_addr);
	for(int i = 0; i < size; i++)
	{
		p_addr[i] = (uint8_t)*((uint8_t *)buf + i);
	}
	return size;
}

/* copy data from vram */
size_t vram_copy_from(const VRAM *vram, void *buf, v_addr addr, size_t size)
{
	if(addr > vram->v_addr + vram->v_size)
	{
		return 0;
	}
	else if(addr + size > vram->v_addr + vram->v_size)
	{
		size = vram->v_addr + vram->v_size - addr;
	}
	uint8_t *p_addr = vram->p_addr + (addr - vram->v_addr);
	for(int i = 0; i < size; i++)
	{
		*((uint8_t *)buf + i) = p_addr[i];
	}
	return size;
}

void vram_resize(VRAM *vram, size_t new_size)
{
	vram->p_addr = realloc(vram->p_addr, new_size);
	vram->v_size = new_size;
}
