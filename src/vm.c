#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <src/vm.h>

struct arg
{
	uint8_t type;
	uint64_t value;
	size_t size;
};

static size_t get_code(VM *vm, void *code, size_t size)
{
	size = vram_copy_from(&vm->ram, code, vm->ip, size);
	vm->ip += size;
	return size;
}

static size_t get_size_by_type(uint8_t type)
{
	if(type == VM_TYPE_VAL8 || type == VM_TYPE_MEM8)
	{
		return 1;
	}
	else if(type == VM_TYPE_VAL16 || type == VM_TYPE_MEM16)
	{
		return 2;
	}
	else if(type == VM_TYPE_VAL32 || type == VM_TYPE_MEM32)
	{
		return 4;
	}
	else if(type == VM_TYPE_VAL64 || type == VM_TYPE_MEM64)
	{
		return 8;
	}
	/* register */
	else
	{
		return 0;
	}
}

static struct arg get_arg(VM *vm)
{
	struct arg arg;
	get_code(vm, &arg.type, sizeof(arg.type));
	arg.value = 0;

	arg.size = get_size_by_type(arg.type);

	if(arg.type >= VM_TYPE_MEM8 && arg.type <= VM_TYPE_MEM64)
	{
		get_code(vm, &arg.value, 8);
	}
	else
	{
		get_code(vm, &arg.value, arg.size);
	}

	return arg;
}

static uint64_t get_value(const VM *vm, const struct arg *arg)
{
	uint64_t value = 0;
	if(arg->type >= VM_TYPE_VAL8 && arg->type <= VM_TYPE_VAL64)
	{
		value = arg->value;
	}
	else if(arg->type >= VM_TYPE_MEM8 && arg->type <= VM_TYPE_MEM64)
	{
		vram_copy_from(&vm->ram, &value, arg->value, get_size_by_type(arg->type));
	}
	else if(arg->type == VM_REG_SP)
	{
		value = vm->sp;
	}
	else if(arg->type == VM_REG_IP)
	{
		value = vm->ip;
	}
	else if(arg->type == VM_REG_C0)
	{
		value = vm->c0;
	}
	return value;
}

static void set_value(VM *vm, const struct arg *arg, uint64_t value)
{
	if(arg->type >= VM_TYPE_MEM8 && arg->type <= VM_TYPE_MEM64)
	{
		vram_copy_to(&vm->ram, &value, arg->value, get_size_by_type(arg->type));
	}
	else if(arg->type == VM_REG_SP)
	{
		vm->sp = value;
	}
	else if(arg->type == VM_REG_IP)
	{
		vm->ip = value;
	}
	else if(arg->type == VM_REG_C0)
	{
		vm->c0 = value;
	}
}

void vm_init(VM *vm)
{
	vm->c0 = 0;
	vm->code = CODE_ADDR;
	vm->stack = STACK_ADDR;
	vram_init(&vm->ram, 0, VRAM_SIZE);
}

void vm_update_code(VM *vm, const void *code, size_t size)
{
	vram_copy_to(&vm->ram, code, CODE_ADDR, size);
	vm->ip = CODE_ADDR;
}

void vm_run(VM *vm)
{
	for(;;)
	{
		uint8_t instr;
		get_code(vm, &instr, sizeof(instr));
		/* 'mov' instruction */
		if(instr == VM_OP_MOV)
		{
			struct arg source = get_arg(vm);
			struct arg target = get_arg(vm);

			uint64_t tmp_data = get_value(vm, &source);
			set_value(vm, &target, tmp_data);
		}
		/* 'jmp' instruction */
		else if(instr == VM_OP_JMP)
		{
			struct arg addr = get_arg(vm);
			vm->ip = get_value(vm, &addr);
		}
		else if(instr >= VM_OP_ADD && instr <= VM_OP_DIV)
		{
			struct arg source = get_arg(vm);
			struct arg value = get_arg(vm);

			uint64_t tmp_data = get_value(vm, &source);
			uint64_t v = get_value(vm, &value);
			if(instr == VM_OP_ADD)
			{
				tmp_data += v;
			}
			else if(instr == VM_OP_SUB)
			{
				tmp_data -= v;
			}
			else if(instr == VM_OP_MUL)
			{
				tmp_data *= v;
			}
			else
			{
				tmp_data /= v;
			}
			set_value(vm, &source, tmp_data);
		}
		/* 'out' instruction */
		else if(instr == VM_OP_OUT)
		{
			struct arg port = get_arg(vm);
			struct arg data = get_arg(vm);
			char c = get_value(vm, &data);
			if(get_value(vm, &port) == VM_DEV_STDOUT)
			{
				write(STDOUT_FILENO, &c, sizeof(char));
			}
			else if(get_value(vm, &port) == VM_DEV_STDERR)
			{
				write(STDERR_FILENO, &c, sizeof(char));
			}
		}
		/* 'in' instruction */
		else if(instr == VM_OP_IN)
		{
			struct arg port = get_arg(vm);
			char c = 0;
			if(get_value(vm, &port) == VM_DEV_STDIN)
			{
				read(STDIN_FILENO, &c, sizeof(char));
			}
			vm->c0 = c;
		}
		/* 'push' instruction */
		else if(instr == VM_OP_PUSH)
		{
			struct arg reg = get_arg(vm);
			vm->sp -= VM_BASE_BYTE;
			uint64_t value = get_value(vm, &reg);
			vram_copy_to(&vm->ram, &value, vm->sp, VM_BASE_BYTE);
		}
		/* 'pop' instruction */
		else if(instr == VM_OP_POP)
		{
			struct arg reg = get_arg(vm);
			uint64_t value;
			vram_copy_from(&vm->ram, &value, vm->sp, VM_BASE_BYTE);
			set_value(vm, &reg, value);
			vm->sp += VM_BASE_BYTE;
		}
		/* 'call' instruction */
		else if(instr == VM_OP_CALL)
		{
			struct arg addr = get_arg(vm);
			
			/* push IP into stack */
			vram_copy_to(&vm->ram, &vm->ip, vm->sp, VM_BASE_BYTE);
			vm->sp -= VM_BASE_BYTE;

			vm->ip = get_value(vm, &addr);
		}
		/* 'ret' instruction */
		else if(instr == VM_OP_RET)
		{
			vram_copy_from(&vm->ram, &vm->ip, vm->sp, VM_BASE_BYTE);
			vm->sp += VM_BASE_BYTE;
		}
		/* 'hal' instruction */
		else if(instr == VM_OP_HAL)
		{
			return;
		}
		/* handling illegal instructions */
		else
		{
			printf("Illegal instruction: 0x%x at 0x%lx\n", instr, vm->ip);
			return;
		}
	}
}
