#include <stdio.h>
#include <string.h>
#include <src/ast.h>
#include <src/token.h>
#include <src/vm.h>

#include "debug.h"

/* print token */
void print_token(const TOKEN tk)
{
	printf("%s %s\n", tk.name, token_types[tk.type]);
}
/* print token list */
void print_tokens(const TOKEN *tk)
{
	int i = 0;
	while(tk[i].type != 0)
	{
		print_token(tk[i]);
		i++;
	}
}
/* print AST */
static void _print_ast_node(const AST_NODE *node, int tab)
{
	for(int j = 0; j < tab; j++)
	{
		printf("\t");
	}
	printf("%s %s\n", ast_types[node->type], node->data);
}

static void _print_ast(const AST_NODE *node, int re)
{
	_print_ast_node(node, re);
	int i = 0;
	while(node->nodes[i] != NULL)
	{
		_print_ast(node->nodes[i], re + 1);
		i++;
	}
}
/* print AST */
void print_ast(const AST_NODE *node)
{
	_print_ast(node, 0);
}

void print_vm(const VM *vm)
{
	printf("C0: %lx\n", vm->c0);
	printf("IP: %lx\n", vm->ip);
}
