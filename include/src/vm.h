#include <stdint.h>
#include <src/vram.h>

#define VM_BITS 64 //64-bit virtual macine
#define VM_BASE_BYTE (VM_BITS / 8) //basic data size

#define CODE_ADDR 0x10000
#define STACK_ADDR 0x20000
#define STACK_SIZE 1024 * 1024

#define VRAM_SIZE 20 * 1024 * 1024 //20MiB

#define VM_OP_MOV	0x01
#define VM_OP_IN	0x02
#define VM_OP_OUT	0x03
#define VM_OP_JMP	0x04
#define VM_OP_CMP	0x05
#define VM_OP_ADD	0x06
#define VM_OP_SUB	0x07
#define VM_OP_MUL	0x08
#define VM_OP_DIV	0x09
#define VM_OP_PUSH	0x0a
#define VM_OP_POP	0x0b
#define VM_OP_CALL	0x0c
#define VM_OP_RET	0x0d
#define VM_OP_HAL	0x1f

#define VM_REG_C0	0x20
#define VM_REG_SP	0x23
#define VM_REG_IP	0x24
#define VM_TYPE_REG	0x25
#define VM_TYPE_VAL8	0x26
#define VM_TYPE_VAL16	0x27
#define VM_TYPE_VAL32	0x28
#define VM_TYPE_VAL64	0x29
#define VM_TYPE_MEM8	0x2a
#define VM_TYPE_MEM16	0x2b
#define VM_TYPE_MEM32	0x2c
#define VM_TYPE_MEM64	0x2d

#define VM_DEV_STDIN	0
#define VM_DEV_STDOUT	1
#define VM_DEV_STDERR	2

typedef struct vm
{
	uint64_t c0, ip, sp;
	VRAM ram;
	v_addr stack, code;
	v_addr *heap;
} VM;

void vm_init(VM *);
void vm_run(VM *);
void vm_update_code(VM *, const void *, size_t);
