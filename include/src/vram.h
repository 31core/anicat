typedef uint64_t v_addr;

typedef struct vram
{
	void *p_addr;
	v_addr v_addr;
	size_t v_size;
} VRAM;

void vram_init(VRAM *, v_addr, size_t);
size_t vram_copy_to(VRAM *, const void *, v_addr, size_t);
size_t vram_copy_from(const VRAM *, void *, v_addr, size_t);
void vram_resize(VRAM *, size_t);
